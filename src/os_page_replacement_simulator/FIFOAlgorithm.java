/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_page_replacement_simulator;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.IntStream;
import javax.swing.JPanel;

/**
 *
 * @author user
 */

public class FIFOAlgorithm {
    int hits = 0;
    float hitRatio = 0;
    void fifoPages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel) {
        int[] fifo = new int[frameSize];
        int hitCount = 0, ptr = 0;
        boolean check;
        for(int i=0; i<pagesList.size(); ++i) {
            int value = pagesList.get(i);
            //checking if value exists in frame
            check = IntStream.of(fifo).anyMatch(x -> x == value);
            
            if(ptr == frameSize) {
                ptr = 0;
            }
            
            if(!check) {
                fifo[ptr] = value;
                ptr++;
            } else if(check){
                this.hits++;
            }
            
            generateFrame(fifo, displayPagePanel, pagesList);         
        }
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }
    
    private void generateFrame(int[] frame, JPanel displayPagePanel, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/5, 5));
        
        PageFrame pf = new PageFrame(frame);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    
    int getHits(){
        return this.hits;
    }
    
    float getHitRatio(){
        return this.hitRatio;
    }
    
}
