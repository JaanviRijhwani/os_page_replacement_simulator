/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_page_replacement_simulator;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.stream.IntStream;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class LRUAlgorithm {
    int hits = 0;
    float hitRatio = 0;
    
    void lruPages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel)
    {
        int[] lru = new int[frameSize];
        int[] dummyArray = new int[pagesList.size()];
        int hitCount = 0, ptr = 0, counter = 0;        
        boolean check1, check2;

        for(int i=0;i<frameSize;i++)
        {        
            lru[i] = -1;
        }
        for(int i=0; i<pagesList.size(); ++i) {
            check1 = check2 = false;
            int value = pagesList.get(i);
            
            for(int j=0; j<frameSize; ++j) {
                if(lru[j] == value) {
                    counter++;
                    dummyArray[j] = counter;
                    check1 = check2 = true;
                    this.hits++;
                    break;
                }
            }
            
            if(!check1)
            {
                for(int j=0;j<frameSize;j++)
                {
                    if(lru[j] == -1)
                    {   
                        lru[j] = value;
                        counter++;
                        dummyArray[j] = counter;
                        check2 = true;
                        break;
                    }
                }
            }
            if(!check2)
            {
                int pos = findLRU(dummyArray, frameSize);
                counter++;
                lru[pos] = value;
                dummyArray[ptr] = counter;
            }
                
            generateFrame(lru, displayPagePanel, pagesList);         
        }
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }
    
    private void generateFrame(int[] frame, JPanel displayPagePanel, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/4, 4));
        
        PageFrame pf = new PageFrame(frame);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    
    int getHits(){
        return this.hits;
    }
    
    float getHitRatio(){
        return this.hitRatio;
    }
    
    private int findLRU(int dummyArray[], int n){
        int i, minimum = dummyArray[0], pos = 0;

        for(i = 1; i < n; ++i){
            if(dummyArray[i] < minimum){
                minimum = dummyArray[i];
                pos = i;
            }
        }
        return pos;
    }

}
