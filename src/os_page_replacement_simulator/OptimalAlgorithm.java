/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_page_replacement_simulator;

import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class OptimalAlgorithm {
    int hits = 0;
    float hitRatio = 0;
    
    void optimalPages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel)
    {
       
        int frame[] = new int[frameSize], temp[] = new int[frameSize];
        boolean flag1, flag2, flag3;
        int index=0, max, hits = 0;
        
        for(int i = 0; i < frameSize; i++){
            frame[i] = -1;
        }
        for(int i=0; i<pagesList.size(); i++){
            flag1 = flag2 = false;
            for(int j = 0; j < frameSize; ++j){
                if(frame[j] == pagesList.get(i)){
                    flag1 = flag2 = true;
                    generateFrame(frame, displayPagePanel, pagesList);
                    this.hits++;
                    break;
                }
            }
            if(!flag1){
                for(int j=0; j<frameSize; ++j){
                    if(frame[j] == -1){
                        frame[j] = pagesList.get(i);
                        flag2 = true;
                        generateFrame(frame, displayPagePanel, pagesList);
                        break;
                    }
                }    
            }
            if(!flag2){
        	flag3 = false;
                for(int j=0; j<frameSize; ++j){
                    temp[j] = -1;

                    for(int k=i+1; k<pagesList.size(); ++k){
                        if(frame[j] == pagesList.get(k)){
                            temp[j]=k;
                            break;
                        }
                    }
                }
                for(int j=0; j<frameSize; ++j){
                    if(temp[j] == -1){
                        index = j;
                        flag3 = true;
                        break;
                    }
                }
                if(!flag3){
                    max = temp[0];
                    index = 0;

                    for(int j=1; j<frameSize; ++j){
                        if(temp[j] > max){
                            max = temp[j];
                            index = j;
                        }
                    }            	
                }
			
                frame[index] = pagesList.get(i);
                generateFrame(frame, displayPagePanel, pagesList);
                
            }
        }
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }
    
    private void generateFrame(int[] frame, JPanel displayPagePanel, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/4, 4));
        
        PageFrame pf = new PageFrame(frame);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    
    int getHits(){
        return this.hits;
    }
    
    float getHitRatio(){
        return this.hitRatio;
    }
}
